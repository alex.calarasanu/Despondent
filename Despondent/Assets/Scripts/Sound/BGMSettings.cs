﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Despondent/BGMSettings")]
public class BGMSettings : ScriptableObject
{
    //[Range (0, 1)]
    //public float piano;
    //[Range (0, 1)]
    //public float bassline;
    //[Range (0, 1)]
    //public float melody;
    //[Range (0, 1)]
    //public float chello;
    //[Range (0, 1)]
    //public float kick;
    //[Range (0, 1)]
    //public float snare;
    //[Range (0, 1)]
    //public float hihat;
    //[Range (0, 1)]
    //public float sel1;
    //[Range (0, 1)]
    //public float sel2;
    //[Range (0, 1)]
    //public float sel3;

    public float[] volumes = new float[10];
}
