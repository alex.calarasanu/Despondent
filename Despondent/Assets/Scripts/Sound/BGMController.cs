﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class BGMController : MonoBehaviour
{
    public AudioMixer mixer;
    public BGMSettings currentSettings;

    private string[] mixerNames = { "Piano", "Bassline", "Bass2", "StretchyMelody", "Chello", "Kick", "Snare", "HiHat", "Sel1", "Sel2", "Sel3" };

    public void FadeBGM (BGMSettings newSettings)
    {
        StopAllCoroutines();
        StartCoroutine (FadeBGMSettings (newSettings));
    }

    private IEnumerator FadeBGMSettings (BGMSettings newSettings)
    {
        float timePassed = 0f;

        while (timePassed < Timings.SCREEN_TRANSITION_TIME * 2)
        {
            float progress = timePassed / (Timings.SCREEN_TRANSITION_TIME * 2);

            for (int i = 0; i < newSettings.volumes.Length; i++)
            {
                float volume = Mathf.Lerp (currentSettings.volumes[i], newSettings.volumes[i], progress);
                mixer.SetFloat (mixerNames[i], volume);
            }

            yield return null;
            timePassed += Time.deltaTime;
        }

        currentSettings = newSettings;
    }
}
