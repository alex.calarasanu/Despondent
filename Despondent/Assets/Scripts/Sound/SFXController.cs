﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXController : MonoBehaviour
{
    [SerializeField]
    private AudioSource dayTransitionAudio;

    private static SFXController instance;

    public static SFXController Instance
    {
        get
        {
            return instance;
        }

        set
        {
            instance = value;
        }
    }

    private void Awake()
    {
        // only allow one GameManager instance at any time
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy (gameObject);
        }

        DontDestroyOnLoad (gameObject);
        GameController.OnEndOfDay += PlayDayTransition;
    }

    public void PlayDayTransition()
    {
        dayTransitionAudio.Play();
    }
}
