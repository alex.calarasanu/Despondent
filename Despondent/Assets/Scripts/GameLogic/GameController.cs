﻿using System.Collections;
using UnityEngine;

public class GameController : MonoBehaviour
{
    // Definition of screen switch event, this will be fired e.g. if the player walk out of his house.
    // Other scripts should subscribe in their Awake method
    public delegate void OnScreenSwitchDelegate (LevelScreen nextScreen, bool showTransition);
    public static event OnScreenSwitchDelegate OnScreenSwitchStart;
    public static event OnScreenSwitchDelegate OnScreenSwitchEventPoint;

    public delegate void OnEndOfDayDelegate();
    public static event OnEndOfDayDelegate OnEndOfDay;

    public static event OnEndOfDayDelegate OnNewDay;

    private float secondsPerDay = 1000f;

    private static GameController instance;

    [SerializeField]
    private LevelScreen currentScreen;

    [SerializeField]
    private GameObject inventoryPanel;
    [SerializeField]
    private InventoryMouseOver inventoryMouseOver;

    public BGMController bgmController;

    private GameObject player;

    private LevelScreen firstScreen;


    private GameObject[] itemsInWorld;


    private void Awake()
    {
        // only allow one GameManager instance at any time
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy (gameObject);
        }

        DontDestroyOnLoad (gameObject);
        firstScreen = currentScreen;

        itemsInWorld = GameObject.FindGameObjectsWithTag ("Item");

    }

    private void Start()
    {

        ScreenSwitch (firstScreen, false);
        StartCoroutine (DayCountdown());

    }

    public void ScreenSwitch (LevelScreen nextScreen, bool showTransition)
    {
        if (nextScreen.bgmSettings != null)
        {
            bgmController.FadeBGM (nextScreen.bgmSettings);
        }

        StartCoroutine (ScreenSwitchEvent (nextScreen, showTransition));
    }

    private IEnumerator ScreenSwitchEvent (LevelScreen nextScreen, bool showTransition)
    {
        if (OnScreenSwitchStart != null)
        {
            // fire that event and tell every script which subscribed to the event about the screen switch
            currentScreen = nextScreen;
            OnScreenSwitchStart (currentScreen, showTransition);

            if (showTransition)
            {
                yield return new WaitForSeconds (Timings.SCREEN_TRANSITION_TIME / 2f);
            }

            OnScreenSwitchEventPoint (nextScreen, showTransition);
        }
    }

    public bool ToggleInventory()
    {
        bool activeState = !inventoryPanel.activeSelf;
        inventoryPanel.SetActive (activeState);

        if (!activeState)
        {
            inventoryMouseOver.DeActivate();
        }

        return activeState;
    }

    private IEnumerator DayCountdown ()
    {
        yield return new WaitForSeconds (secondsPerDay);

        if (OnEndOfDay != null)
        {
            OnEndOfDay();
        }

        yield return new WaitForSeconds (Timings.DAY_TRANSITION_TIME);

        ScreenSwitch (firstScreen, false);
        inventoryPanel.SetActive (false);
        inventoryMouseOver.DeActivate();

        foreach (GameObject item in itemsInWorld)
        {
            item.GetComponent<ItemInGameWorld>().SetEnabled (true);
        }

        Inventory.Clear();

        if (OnNewDay != null)
        {
            OnNewDay();
        }
    }
}
