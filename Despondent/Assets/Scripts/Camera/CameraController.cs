﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof (Camera))]
public class CameraController : MonoBehaviour
{
    private Camera gameCamera;
    private LevelScreen currentScreen;
    private Transform playerTransform;

    private bool screenTransitionActive = false;

    [SerializeField]
    private Image screenBlackImage;

    private void Awake()
    {
        gameCamera = GetComponent<Camera>();
        GameController.OnScreenSwitchStart += OnScreenSwitch;
        GameController.OnEndOfDay += OnEndOfDay;
        playerTransform = FindObjectOfType<PlayerController>().gameObject.transform;
    }

    // Update is called once per frame
    void Update ()
    {
        if (screenTransitionActive)
        {
            return;
        }

        transform.position = CalculateTargetPosition();
    }

    private Vector3 CalculateTargetPosition()
    {
        float aspect = (float) Screen.width / (float) Screen.height;
        float camExtendX = gameCamera.orthographicSize * aspect;
        float camExtendY = gameCamera.orthographicSize;
        Bounds screenBounds = currentScreen.Bounds;

        if (screenBounds.extents.x <= camExtendX)
        {
            // we cannot scroll if the current level screen is not as wide as the camera
            // so just keep it in the center
            return new Vector3 (screenBounds.center.x, screenBounds.center.y, transform.position.z);
        }

        // let the camera follow the player's x position, but keep it within the
        // current level screen's bounds
        float minX = screenBounds.center.x - screenBounds.extents.x + camExtendX;
        float maxX = screenBounds.center.x + screenBounds.extents.x - camExtendX;

        float minY = screenBounds.center.y - screenBounds.extents.y + camExtendY;
        float maxY = screenBounds.center.y + screenBounds.extents.y - camExtendY;

        float targetX = Mathf.Clamp (playerTransform.position.x, minX, maxX);
        float targetY = Mathf.Clamp (playerTransform.position.y, minY, maxY);

        return new Vector3 (targetX, targetY, transform.position.z);
    }

    private void OnEndOfDay()
    {
        StartCoroutine (EndOfDay());
    }

    private void OnScreenSwitch (LevelScreen nextScreen, bool showTransition)
    {
        currentScreen = nextScreen;

        if (showTransition)
        {
            StartCoroutine (ScreenTransition());
        }
    }

    private IEnumerator ScreenTransition()
    {
        screenTransitionActive = true;

        float timeElapsed = 0f;

        Vector3 targetPosition = CalculateTargetPosition();

        Color c = screenBlackImage.color;

        float halfTransitionTime = Timings.SCREEN_TRANSITION_TIME / 2f;

        while (timeElapsed < halfTransitionTime)
        {
            // pause until next frame
            yield return null;
            timeElapsed += Time.deltaTime;

            c.a = timeElapsed / halfTransitionTime;
            screenBlackImage.color = c;
        }

        transform.position = targetPosition;
        timeElapsed = 0f;

        while (timeElapsed < halfTransitionTime)
        {
            // pause until next frame
            yield return null;
            timeElapsed += Time.deltaTime;

            c.a = 1f - (timeElapsed / halfTransitionTime);
            screenBlackImage.color = c;
        }

        c.a = 0f;
        screenBlackImage.color = c;

        screenTransitionActive = false;
    }

    private IEnumerator EndOfDay()
    {
        float timeElapsed = 0f;

        Vector3 targetPosition = CalculateTargetPosition();

        Color c = screenBlackImage.color;

        while (timeElapsed < Timings.DAY_TRANSITION_TIME)
        {
            // pause until next frame
            yield return null;
            timeElapsed += Time.deltaTime;

            c.a = timeElapsed / Timings.DAY_TRANSITION_TIME;
            screenBlackImage.color = c;
        }

        float halfTransitionTime = Timings.SCREEN_TRANSITION_TIME / 2f;

        while (timeElapsed < halfTransitionTime)
        {
            // pause until next frame
            yield return null;
            timeElapsed += Time.deltaTime;

            c.a = timeElapsed / halfTransitionTime;
            screenBlackImage.color = c;
        }

        c.a = 0f;
        screenBlackImage.color = c;
    }
}
