﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootstepSoundController : MonoBehaviour
{

    [SerializeField]
    private AudioSource leftStep;
    [SerializeField]
    private AudioSource rightStep;

    private void Awake()
    {
        GameController.OnScreenSwitchEventPoint += OnScreenSwitch;
    }

    public void OnScreenSwitch (LevelScreen nextScreen, bool showTransition)
    {
        leftStep.clip = nextScreen.footStepLeft;
        rightStep.clip = nextScreen.footStepRight;
    }

    public void OnLeftFootStep()
    {
        leftStep.Play();
    }

    public void OnRightFootStep()
    {
        rightStep.Play();
    }

}
