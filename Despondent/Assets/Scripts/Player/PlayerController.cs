﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Rigidbody2D))]
public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float speed;

    private Rigidbody2D rb2d;
    private GameController gameController;

    [SerializeField]
    private Animator[] animators;

    private Door doorBehindCharacter = null;
    private bool doorTransitionActive = false;
    private bool screenTransitionActive = false;

    private bool inventoryActive = false;

    [SerializeField]
    private GameObject spriteLeft;
    [SerializeField]
    private GameObject spriteRight;

    private Vector3 startPosition;
    [HideInInspector]
    public int DepressionMeter = 10;
    public bool HasTakenPillsToday
    {
        get;
        set;
    }
    private void Awake()
    {
        gameController = FindObjectOfType<GameController>();
        startPosition = transform.position;
        GameController.OnNewDay += OnNewDayResetPosition;
        GameController.OnScreenSwitchEventPoint += OnScreenSwitch;
    }

    // Use this for initialization
    void Start ()
    {
        rb2d = GetComponent<Rigidbody2D>();

    }

    private void Update()
    {
        if (screenTransitionActive || DialogController.Instance.IsDialogActive)
        {
            // don't take any input during screen transitions and dialogs
            rb2d.velocity = Vector2.zero;
            return;
        }

        if (Input.GetButtonDown ("Inventory"))
        {
            inventoryActive = gameController.ToggleInventory();
        }
    }

    // the cycle in which all physics simulations are calculated
    // it is safe to modify physics properties in here
    void FixedUpdate ()
    {
        if (inventoryActive || screenTransitionActive)
        {
            // don't take any input during screen transitions
            rb2d.velocity = Vector2.zero;
            return;
        }

        float horizontal = Input.GetAxis ("Horizontal");
        float vertical = Input.GetAxis ("Vertical");

        // we want instant response so we are directly working with the physics body's velocity
        // rather than applying a force vector
        rb2d.velocity = new Vector2 (speed * horizontal, rb2d.velocity.y);

        if (horizontal < 0)
        {
            spriteLeft.SetActive (true);
            spriteRight.SetActive (false);
        }
        else if (horizontal > 0)
        {
            spriteLeft.SetActive (false);
            spriteRight.SetActive (true);
        }

        foreach (Animator anim in animators)
        {
            if (anim.gameObject.activeSelf)
            {
                anim.SetFloat ("hSpeed", Mathf.Abs (horizontal));
            }
        }

        if (vertical > 0f && !doorTransitionActive)
        {
            // press upward to enter the door behind the character
            StartCoroutine (EnterDoor());
        }
    }

    private void OnNewDayResetPosition()
    {
        if (!HasTakenPillsToday)
        {
            DepressionMeter += 15;
            Debug.Log ("Deppresion meter = " + DepressionMeter);
        }
        else
        {
            DepressionMeter -= 5;
            HasTakenPillsToday = false;
            Debug.Log ("Deppresion meter = " + DepressionMeter);
        }

        StopAllCoroutines();
        transform.position = startPosition;
    }

    private void OnScreenSwitch (LevelScreen nextScreen, bool showTransition)
    {
        transform.localScale = new Vector3 (nextScreen.characterScale, nextScreen.characterScale, nextScreen.characterScale);
    }

    #region Door Mechanic
    private IEnumerator EnterDoor()
    {
        if (doorBehindCharacter != null)
            if (doorBehindCharacter.isDoorLocked == false)
            {
                {
                    doorTransitionActive = true;

                    // wait for fixed update, since we are modifying an object with physics properties.
                    yield return new WaitForFixedUpdate();

                    Door exitDoor = doorBehindCharacter.Other;

                    // set the position through the physic body rather than the game object's transform
                    // to prevent glitches in the simulation.
                    rb2d.position = exitDoor.Position;
                    rb2d.velocity = Vector2.zero;
                    doorBehindCharacter = null;

                    // if we change the screen with that door, we gotta tell the game controller
                    if (exitDoor.Type == Door.DoorType.ScreenChange)
                    {
                        screenTransitionActive = true;
                        gameController.ScreenSwitch (exitDoor.Screen, true);

                        // wait for end of screen transition animation.
                        // the player should not have control during screen screen transitions.
                        yield return new WaitForSeconds (Timings.SCREEN_TRANSITION_TIME);

                        screenTransitionActive = false;
                    }
                    else
                    {
                        // wait a short amount of time until we enable walking through doors again
                        // to prevent flickering between two linked doors when keeping "up" pressed.
                        yield return new WaitForSeconds (Timings.DOOR_ENTER_COOLDOWN);
                    }

                    doorBehindCharacter = exitDoor;
                    doorTransitionActive = false;


                    //Check if player forgot to pickup the key before leaving the house
                    bool hasPickedUpKey = false;
                    Inventory inv = FindObjectOfType<Inventory>();

                    for (int i = 0; i < inv.ItemsInInventory.Length; i++)
                    {
                        if (inv.ItemsInInventory[i] != null && inv.ItemsInInventory[i].itemName == "Key")
                        {
                            hasPickedUpKey = true;
                            Debug.Log ("Player did not forget key");
                            break;
                        }
                    }

                    if (!hasPickedUpKey)
                    {
                        DepressionMeter += 10;
                        Debug.Log ("Player forgot the key || Deppresion Meter: " + DepressionMeter);
                    }

                }
            }
    }

    private void OnTriggerEnter2D (Collider2D collision)
    {
        if (collision.tag == TagNames.DOOR)
        {
            // whenever the player enters a door trigger, keep the door instance in
            // case he presses the "up" button
            Door door = collision.gameObject.GetComponent<Door>();
            doorBehindCharacter = door;
        }
    }

    private void OnTriggerExit2D (Collider2D collision)
    {
        if (collision.tag == TagNames.DOOR)
        {
            // clear the saved door instance when the player leaves the door area
            doorBehindCharacter = null;
        }
    }
    #endregion
}
