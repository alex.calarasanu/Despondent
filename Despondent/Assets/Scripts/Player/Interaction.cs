﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction : MonoBehaviour
{
    [SerializeField]
    private LayerMask interactionLayers;

    private List<ItemInGameWorld> itemsInRange = new List<ItemInGameWorld>();
    private List<Person> personsInRange = new List<Person>();

    private void Update()
    {
        if (Input.GetMouseButtonDown (0))
        {
            CheckInteraction();
        }
    }

    private void CheckInteraction()
    {
        Vector3 worldPoint = Camera.main.ScreenToWorldPoint (Input.mousePosition);
        Vector2 origin = new Vector2 (worldPoint.x, worldPoint.x);
        RaycastHit2D hit = Physics2D.Raycast (worldPoint, Vector2.zero, 0f, interactionLayers);

        if (hit.collider != null)
        {
            if (hit.collider.CompareTag (TagNames.ITEM))
            {
                //we have clicked on an item, get the items instance
                ItemInGameWorld itemInGame = hit.collider.gameObject.GetComponent<ItemInGameWorld>();

                //only pick it up if it's within our pickup radius
                if (itemsInRange.Contains (itemInGame))
                {
                    Inventory.PickUp (itemInGame);
                }
            }
            else if (hit.collider.CompareTag (TagNames.PERSON))
            {
                Person person = hit.collider.gameObject.GetComponent<Person>();

                if (personsInRange.Contains (person))
                {
                    person.Interact();
                }
            }
        }
    }

    /// <summary>
    /// Interaction radius enter trigger.
    /// </summary>
    private void OnTriggerEnter2D (Collider2D collision)
    {
        if (collision.CompareTag (TagNames.ITEM))
        {
            ItemInGameWorld itemInGame = collision.gameObject.GetComponent<ItemInGameWorld>();
            itemsInRange.Add (itemInGame);
        }
        else if (collision.CompareTag (TagNames.PERSON))
        {
            Person person = collision.gameObject.GetComponent<Person>();
            personsInRange.Add (person);
        }
    }

    /// <summary>
    /// Interaction radius exit trigger.
    /// </summary>
    private void OnTriggerExit2D (Collider2D collision)
    {
        if (collision.CompareTag (TagNames.ITEM))
        {
            ItemInGameWorld itemInGame = collision.gameObject.GetComponent<ItemInGameWorld>();
            itemsInRange.Remove (itemInGame);
        }
        else if (collision.CompareTag (TagNames.PERSON))
        {
            Person person = collision.gameObject.GetComponent<Person>();
            personsInRange.Remove (person);
        }
    }
}
