﻿using UnityEngine;

/// <summary>
/// Property representation of an item. You can create a new one in the context menu Create->Despondent->Item
/// The icon will be used in the game world as well as the inventory UI.
/// </summary>
[CreateAssetMenu (menuName = "Despondent/Item")]
public class Item : ScriptableObject
{
    public enum ItemType {Key, Note, Medication};
    public ItemType itemType;
    public Sprite icon;
    public string itemName;
    [TextArea]
    public string itemDescription;
    public AudioClip itemSound;
}
