﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Renderer to display an item within the game world.
/// </summary>
[RequireComponent (typeof (SpriteRenderer))]
public class ItemInGameWorld : MonoBehaviour
{
    public Item item;
    private SpriteRenderer spriteRenderer;
    private CircleCollider2D collider;

    /// <summary>
    /// Gets called whenever a property is changed in the Editor Inspector panel
    /// </summary>
    private void OnValidate()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        collider = GetComponent<CircleCollider2D>();

        if (item != null)
        {
            spriteRenderer.sprite = item.icon;
        }
        else
        {
            spriteRenderer.sprite = null;
        }
    }

    public void SetEnabled (bool enabled)
    {
        spriteRenderer.enabled = enabled;
        collider.enabled = enabled;
    }
}
