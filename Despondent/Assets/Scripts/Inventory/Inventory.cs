﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    private static Inventory instance;

    private static readonly int ITEM_SLOT_NR_FULL = -1;

    [SerializeField]
    private ItemSlot[] itemSlots;

    private Item[] itemsInInventory;
    public Item[] ItemsInInventory
    {
        get
        {
            return itemsInInventory;
        }
    }
    private Item selectedItem = null;

    private AudioSource audioSource;

    private void Awake()
    {
        // Singleton
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy (gameObject);
        }

        DontDestroyOnLoad (gameObject);

        itemsInInventory = new Item[itemSlots.Length];
        audioSource = GetComponent<AudioSource>();
    }

    public void Update()
    {
        if (selectedItem != null)
        {
            if (Input.GetMouseButtonDown (0))
            {

                Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
                RaycastHit2D hitInfo = Physics2D.Raycast (ray.origin, ray.direction, Mathf.Infinity);

                if (hitInfo)
                {
                    Debug.Log ("HIT:" + hitInfo.collider.tag.ToString());

                    switch (selectedItem.itemType)
                    {
                        case Item.ItemType.Key:
                            UseKey (hitInfo);
                            break;

                        case Item.ItemType.Note:
                            UseKey (hitInfo);
                            break;

                        case Item.ItemType.Medication:
                            UseMedication (hitInfo);
                            break;

                        default:
                            break;
                    }

                    selectedItem = null;
                }

            }

        }
    }

    private int GetNextFreeSlot()
    {
        for (int i = 0; i < itemsInInventory.Length; i++)
        {
            if (itemsInInventory[i] == null)
            {
                return i;
            }
        }

        return ITEM_SLOT_NR_FULL;
    }

    private void PickUpItem (Item itemToPickUp)
    {
        int slotNr = GetNextFreeSlot();

        if (slotNr != ITEM_SLOT_NR_FULL)
        {
            itemsInInventory[slotNr] = itemToPickUp;
            itemSlots[slotNr].PutItem (itemToPickUp);
        }
    }

    public static void PickUp (ItemInGameWorld itemToPickUp)
    {
        //since inventory is singleton we don't need an instance reference to use it
        instance.PickUpItem (itemToPickUp.item);
        itemToPickUp.SetEnabled (false);
    }

    public static void Clear()
    {
        instance.ClearInventory();
    }

    private void ClearInventory()
    {
        for (int i = 0; i < itemsInInventory.Length; i++)
        {
            if (itemsInInventory[i] != null)
            {
                itemsInInventory[i] = null;
                itemSlots[i].TakeItem();
            }
        }
    }

    public void OnItemSelected (int itemSlotSelected)
    {
        if (itemsInInventory[itemSlotSelected] == null)
        {
            selectedItem = null;
            return;
        }

        Debug.Log ("Selected item in slot:" + itemSlotSelected);
        selectedItem = itemsInInventory[itemSlotSelected];
        Debug.Log ("Selected Item:" + selectedItem.itemName);
        audioSource.Stop();
        audioSource.clip = selectedItem.itemSound;
        audioSource.Play();
    }

    public void UseKey (RaycastHit2D hit)
    {
        {
            if (hit.collider.tag == "Door")
            {
                Door door = hit.collider.gameObject.GetComponent (typeof (Door)) as Door;
                door.isDoorLocked = false;
                Debug.Log ("Door is now " + door.isDoorLocked.ToString());
            }
        }
    }
    public void UseMedication (RaycastHit2D hit)
    {
        {
            if (hit.collider.tag == "Player")
            {
                PlayerController p = FindObjectOfType<PlayerController>();
                p.HasTakenPillsToday = true;
                Debug.Log ("Medication taken" + p.HasTakenPillsToday);
            }
        }
    }

}
