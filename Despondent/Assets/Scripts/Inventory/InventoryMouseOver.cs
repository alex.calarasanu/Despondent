﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Controls the position and content of the text field displayed when hovering over an item within the inventory.
/// </summary>
public class InventoryMouseOver : MonoBehaviour
{
    [SerializeField]
    private Text itemName;
    [SerializeField]
    private Text itemDescription;

    private float resolutionScale;

    void Start()
    {
        resolutionScale = (GetComponentInParent<CanvasScaler>().gameObject.transform as RectTransform).localScale.y;
    }

    void Update()
    {
        UpdatePosition();
    }

    void UpdatePosition()
    {
        Vector3 mousePos = Input.mousePosition;
        transform.position = mousePos - new Vector3 (0f, 80f, 0f) * resolutionScale;
        
    }

    public void Activate (Item itemInSlot)
    {
        UpdatePosition();
        itemName.text = itemInSlot.itemName;
        itemDescription.text = itemInSlot.itemDescription;
        gameObject.SetActive (true);
    }

    public void DeActivate()
    {
        gameObject.SetActive (false);
    }
}
