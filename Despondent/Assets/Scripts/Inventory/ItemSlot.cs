﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemSlot : MonoBehaviour
{
    [SerializeField]
    private Image slotUiImage;

    [SerializeField]
    private InventoryMouseOver inventoryMouseOver;

    private Item itemInSlot;
    private EventTrigger eventTrigger;

    private void Awake()
    {
        SetUpEvenTriggers();
    }

    private void SetUpEvenTriggers()
    {
        // Attach mouse enter and exit triggers for the item slot
        eventTrigger = GetComponent<EventTrigger>();

        EventTrigger.Entry entryPointerEnter = new EventTrigger.Entry();
        entryPointerEnter.eventID = EventTriggerType.PointerEnter;
        entryPointerEnter.callback.AddListener ( (eventData) =>
        {
            SetMouseOverActive (true);
        });
        eventTrigger.triggers.Add (entryPointerEnter);

        EventTrigger.Entry entryPointerExit = new EventTrigger.Entry();
        entryPointerExit.eventID = EventTriggerType.PointerExit;
        entryPointerExit.callback.AddListener ( (eventData) =>
        {
            SetMouseOverActive (false);
        });
        eventTrigger.triggers.Add (entryPointerExit);
    }

    public void SetMouseOverActive (bool active)
    {
        if (active && itemInSlot != null)
        {
            inventoryMouseOver.Activate (itemInSlot);
        }
        else
        {
            inventoryMouseOver.DeActivate();
        }
    }

    public void PutItem (Item item)
    {
        if (itemInSlot != null)
        {
            return;
        }

        itemInSlot = item;
        slotUiImage.sprite = itemInSlot.icon;
        slotUiImage.preserveAspect = true;
        slotUiImage.enabled = true;
    }

    public Item TakeItem()
    {
        Item item = itemInSlot;
        slotUiImage.sprite = null;
        slotUiImage.enabled = false;
        itemInSlot = null;
        return item;
    }

}
