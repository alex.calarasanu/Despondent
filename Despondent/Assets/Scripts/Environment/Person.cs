﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Person : MonoBehaviour
{
    [SerializeField]
    private Dialog dialog;

    public void Interact()
    {
        if (!DialogController.Instance.IsDialogActive)
        {
            DialogController.Instance.ShowDialog (dialog);
        }
    }
}
