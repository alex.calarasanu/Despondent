﻿using UnityEngine;

public class LevelScreen : MonoBehaviour
{
    [SerializeField]
    private float extendsX;

    [SerializeField]
    private float extendsY;

    public AudioClip footStepLeft;
    public AudioClip footStepRight;

    public BGMSettings bgmSettings;

    public float characterScale = 1f;

    private Bounds roomBounds;


    public Sprite[] levelSprites;
    public SpriteRenderer spriteRenderer;

    // Use this for initialization
    void Awake ()
    {

        extendsX *= gameObject.transform.localScale.x;
        extendsY *= gameObject.transform.localScale.y;
        roomBounds = new Bounds (gameObject.transform.position, new Vector3 (extendsX * 2f, extendsY * 2f, 0f));
        SetLevelSprite();

    }

    public Bounds Bounds
    {
        get
        {
            return roomBounds;
        }
    }

    private void OnDrawGizmos()
    {
        DrawGizmosInColor (Color.white);
    }

    private void OnDrawGizmosSelected()
    {
        DrawGizmosInColor (Color.red);
    }

    private void DrawGizmosInColor (Color c)
    {
        // draw the dimensions of the screen in the editor window
        Vector3 lowerLeftCorner = gameObject.transform.position - new Vector3 (extendsX, extendsY);
        Vector3 upperRightCorner = gameObject.transform.position + new Vector3 (extendsX, extendsY);
        Vector3 upperLeftCorner = new Vector3 (lowerLeftCorner.x, upperRightCorner.y, 0f);
        Vector3 lowerRightCorner = new Vector3 (upperRightCorner.x, lowerLeftCorner.y, 0f);

        Gizmos.color = c;
        Gizmos.DrawLine (lowerLeftCorner, upperLeftCorner);
        Gizmos.DrawLine (upperLeftCorner, upperRightCorner);
        Gizmos.DrawLine (upperRightCorner, lowerRightCorner);
        Gizmos.DrawLine (lowerRightCorner, lowerLeftCorner);
    }
    private void SetLevelSprite()
    {
        if (levelSprites.Length != 3)
        {
            return;
        }

        PlayerController p = FindObjectOfType<PlayerController>();

        if (p.DepressionMeter < 25)
        {
            spriteRenderer.sprite = levelSprites[0];
        }
        else if (p.DepressionMeter < 75)
        {
            spriteRenderer.sprite = levelSprites[1];
        }
        else
        {
            spriteRenderer.sprite = levelSprites[2];
        }
    }
}
