﻿using UnityEngine;

public class Door : MonoBehaviour
{
    public enum DoorType
    {
        Normal,
        ScreenChange
    }

    [SerializeField]
    private DoorType type;
    [SerializeField]
    private Door other;

    private LevelScreen screen;

    public bool isDoorLocked;

    #region Properties
    public Door Other
    {
        get
        {
            return other;
        }
    }
    public DoorType Type
    {
        get
        {
            return type;
        }
    }
    public LevelScreen Screen
    {
        get
        {
            return screen;
        }
    }
    public Vector3 Position
    {
        get
        {
            return gameObject.transform.position;
        }
    }
    #endregion

    private void Awake()
    {
        screen = GetComponentInParent<LevelScreen>();
    }

    private void OnDrawGizmosSelected()
    {
        // this helps to easily spot which doors are linked in the editor window
        if (other != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine (other.Position, transform.position);
        }
    }
}
