﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Despondent/Dialog")]
public class Dialog : ScriptableObject
{
    [Serializable]
    public class DialogAnswer
    {
        public string answerText;
        public Dialog answerDialog;
    }

    [TextArea]
    public string text;

    public DialogAnswer[] answers;
}
