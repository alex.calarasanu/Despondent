﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogController : MonoBehaviour
{

    [SerializeField]
    private GameObject dialogBox;

    [SerializeField]
    private GameObject dialogContent;

    [SerializeField]
    private Text dialogText;

    [SerializeField]
    private Image avatar;

    [SerializeField]
    private GameObject[] dialogAnswerButtons;

    private Dialog currentDialog;
    private bool isDialogActive;

    private static DialogController instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy (gameObject);
        }

        DontDestroyOnLoad (gameObject);
        GameController.OnNewDay += OnNewDay;
    }

    public static DialogController Instance
    {
        get
        {
            return instance;
        }
    }

    public bool IsDialogActive
    {
        get
        {
            return isDialogActive;
        }
    }

    public void ShowDialog (Dialog dialog)
    {
        currentDialog = dialog;
        dialogText.text = dialog.text;

        int i = 0;

        foreach (GameObject answer in dialogAnswerButtons)
        {
            answer.SetActive (false);
            answer.GetComponent<DialogAnswer>().AnswerDialog = null;
        }

        if (dialog.answers.Length == 0)
        {
            GameObject newAnswer = dialogAnswerButtons[0];
            Text answerText = newAnswer.GetComponentInChildren<Text>();
            answerText.text = "> CLOSE";
            newAnswer.SetActive (true);
        }
        else
            foreach (Dialog.DialogAnswer answer in dialog.answers)
            {
                GameObject newAnswer = dialogAnswerButtons[i];
                //newAnswer.transform.parent = dialogContent.transform;
                //newAnswer.transform.localScale = Vector3.one;
                Text answerText = newAnswer.GetComponentInChildren<Text>();
                answerText.text = "> " + answer.answerText;
                newAnswer.GetComponent<DialogAnswer>().AnswerDialog = answer.answerDialog;
                newAnswer.SetActive (true);
                i++;
            }

        isDialogActive = true;
        dialogBox.SetActive (true);
    }

    public void HideDialog()
    {
        dialogBox.SetActive (false);
        isDialogActive = false;
        currentDialog = null;
    }

    private void OnNewDay()
    {
        HideDialog();
    }
}
