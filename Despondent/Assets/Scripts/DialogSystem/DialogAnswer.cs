﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogAnswer : MonoBehaviour
{
    private Dialog answerDialog;

    public Dialog AnswerDialog
    {
        get
        {
            return answerDialog;
        }

        set
        {
            answerDialog = value;
        }
    }

    public void OnAnswerClick()
    {
        if (answerDialog == null)
        {
            DialogController.Instance.HideDialog();
        }
        else
        {
            DialogController.Instance.ShowDialog (answerDialog);
        }
    }
}
