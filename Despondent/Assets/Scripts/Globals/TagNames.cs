﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TagNames
{
    public static readonly string DOOR = "Door";
    public static readonly string ITEM = "Item";
    public static readonly string PERSON = "Person";
}
