﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timings
{
    // waiting time in seconds before enter a door again
    public static readonly float DOOR_ENTER_COOLDOWN = 0.5f;

    // time for the transition between level screens
    public static readonly float SCREEN_TRANSITION_TIME = 1.0f;

    public static readonly float DAY_TRANSITION_TIME = 4.4f;
}
